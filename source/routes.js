import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import StudentsPage from './components/student/StudentsPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={StudentsPage} />
  </Route>
);
