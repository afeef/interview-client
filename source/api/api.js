import API_URL from './config';

const request = require('axios');

const defaultHandle = (response) => {
  if (response.status < 400) {
    return Promise.resolve(response.data);
  }

  return Promise.reject(response.data);
};

class Api {
  static getAllStudents() {
    return request.get(
      `${API_URL}/api/students/`
    ).then(defaultHandle);
  }

  static createStudent(student) {
    return request.post(
      `${API_URL}/api/students/`, student
    ).then(defaultHandle);
  }

  static updateStudent(student) {
    return request.put(
      `${API_URL}/api/students/${student.ID}/`, student
    ).then(defaultHandle);
  }

  static getAllNationalities() {
    return request.get(
      `${API_URL}/api/nationalities`
    ).then(defaultHandle);
  }

  static getStudentNationality(studentId, nationalityId) {
    return request.get(
      `${API_URL}/api/students/${studentId}/nationality/${nationalityId}`
    ).then(defaultHandle);
  }

  static updateStudentNationality(studentId, nationality) {
    return request.put(
      `${API_URL}/api/students/${studentId}/nationality/${nationality.ID}`, nationality
    ).then(defaultHandle);
  }

  static getStudentFamilyMembers(studentId){
    return request.get(
      `${API_URL}/api/students/${studentId}/familymembers/`
    ).then(defaultHandle);
  }

  static createStudentFamilyMember(studentId, familyMember){
    return request.post(
      `${API_URL}/api/students/${studentId}/familymembers/`, familyMember
    ).then(defaultHandle);
  }

  static updateFamilyMember(familyMember){
    return request.put(
      `${API_URL}/api/familymembers/${familyMember.ID}/`, familyMember
    ).then(defaultHandle);
  }

  static deleteFamilyMember(familyMemberId){
    return request.delete(
      `${API_URL}/api/familymembers/${familyMemberId}/`
    ).then(defaultHandle);
  }

  static getFamilyMemberNationality(familyMemberId, nationalityId){
    return request.get(
      `${API_URL}/api/familymembers/${familyMemberId}/nationality/${nationalityId}`
    ).then(defaultHandle);
  }

  static updateFamilyMemberNationality(familyMemberId, nationality){
    return request.put(
      `${API_URL}/api/familymembers/${familyMemberId}/nationality/${nationality.ID}`, nationality
    ).then(defaultHandle);
  }

}

export default Api;
