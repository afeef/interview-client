import * as types from './actionTypes';
import api from '../api/api';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';

export function loadStudentsSuccess(students) {
  return { type: types.LOAD_STUDENTS_SUCCESS, students };
}

export function updateStudentSuccess(student){
  return { type: types.UPDATE_STUDENT_SUCCESS, student };
}

export function createStudentSuccess(student){
  return { type: types.CREATE_STUDENT_SUCCESS, student };
}

export function loadStudentFamilyMembersSuccess(familyMembers) {
  return { type: types.LOAD_STUDENT_FAMILYMEMBERS_SUCCESS, familyMembers};
}

export function addNewFamilyMemberSuccess(familyMember) {
  return { type: types.ADD_STUDENT_FAMILYMEMBER_SUCCESS, familyMember};
}

export function deleteFamilyMemberSuccess(familyMember) {
  return { type: types.DELETE_STUDENT_FAMILYMEMBER_SUCCESS, familyMember};
}

export function loadStudents () {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    return api.getAllStudents().then(students => {
      dispatch(loadStudentsSuccess(students));
    }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}

export function saveStudent(student) {
  return (dispatch, getState) => {
    dispatch(beginAjaxCall());
    if (student.ID){
      return api.updateStudent(student).then(savedStudent => {
         dispatch(updateStudentSuccess(savedStudent));
      }).catch(error => {
        dispatch(ajaxCallError(error));
        throw (error);
      });
    } else {
      return api.createStudent(student).then(updatedStudent => {
        dispatch(createStudentSuccess(updatedStudent));
      }).catch(error => {
        dispatch(ajaxCallError(error));
        throw (error);
      });
    }
  };
}

export function loadStudentFamilyMembers (studentId) {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    return api.getStudentFamilyMembers(studentId).then(familymembers => {
      dispatch(loadStudentFamilyMembersSuccess(familymembers));
    }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}

export function addNewFamilyMember(){
  return (dispatch) => {
    const familyMember = {
      ID:0,
      firstName:'',
      lastName:'',
      dateOfBirth:'',
      relationship:'',
      nationality:{
        ID:0,Title:''
      }
    };

    return dispatch(addNewFamilyMemberSuccess( familyMember ));
  };
}

export function deleteFamilyMember(idx){
  return (dispatch, getState) => {
    const familyMember = getState().familyMembers.filter((s, _idx) => _idx === parseInt(idx))[0];
    return dispatch(deleteFamilyMemberSuccess( familyMember ));
  };
}
