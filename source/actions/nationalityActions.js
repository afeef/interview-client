import * as types from './actionTypes';
import api from '../api/api';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';

export function loadNationalitiesSuccess(nationalities) {
  return { type: types.LOAD_NATIONALITIES_SUCCESS, nationalities};
}

export function loadNationalities() {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    return api.getAllNationalities().then(nationalities => {
      dispatch(loadNationalitiesSuccess(nationalities));
    }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}
