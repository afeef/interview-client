import * as types from './actionTypes';

export function switchUserSuccess(username) {
  return { type: types.SWITCH_USER, username };
}

export function switchUser(username){
  return (dispatch) => {
    return dispatch(switchUserSuccess( username ));
  };
}
