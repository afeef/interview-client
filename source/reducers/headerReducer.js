import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default (state = initialState.username, action) => {
  switch (action.type) {
    case types.SWITCH_USER:
      return action.username;
    default:
      return state;
  }
};
