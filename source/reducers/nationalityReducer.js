import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default (state = initialState.nationalities, action) => {
  switch (action.type) {
    case types.LOAD_NATIONALITIES_SUCCESS:
      return action.nationalities;
    default:
      return state;
  }
};
