import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default (state = initialState.students, action) => {
  switch (action.type) {
    case types.LOAD_STUDENTS_SUCCESS:
      return action.students;
    case types.CREATE_STUDENT_SUCCESS:
      return [
        ...state,
        Object.assign({}, action.student)
      ];

    case types.UPDATE_STUDENT_SUCCESS:
      return [
        ...state.filter(student => student.ID !== action.student.ID),
        Object.assign({}, action.student)
      ];
    default:
      return state;
  }
};
