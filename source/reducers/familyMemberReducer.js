import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default (state = initialState.familyMembers, action) => {
  switch (action.type) {
    case types.LOAD_STUDENT_FAMILYMEMBERS_SUCCESS:
      // if API call return empty result then return an empty array
      return action.familyMembers ? action.familyMembers : [];
    case types.ADD_STUDENT_FAMILYMEMBER_SUCCESS:
    return [
      ...state,
      Object.assign({}, action.familyMember)
    ];
    case types.DELETE_STUDENT_FAMILYMEMBER_SUCCESS:
      return state.filter(familyMember => familyMember.ID !== action.familyMember.ID);
    default:
      return state;
  }
};
