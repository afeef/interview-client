import { combineReducers } from 'redux';
import students from './studentReducer';
import familyMembers from './familyMemberReducer';
import nationalities from './nationalityReducer';
import ajaxCallsInProgress from './ajaxStatusReducer';
import header from './headerReducer';

const rootReducer = combineReducers({
  students,
  familyMembers,
  nationalities,
  ajaxCallsInProgress,
  header
});

export default rootReducer;
