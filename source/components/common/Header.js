import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';
import { Nav, Navbar, NavDropdown, MenuItem } from 'react-bootstrap';

const Header = ({username, onUserChange}) => {
  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <IndexLink to="/" activeClassName="active">Home</IndexLink>
        </Navbar.Brand>
      </Navbar.Header>
      <Nav>
        <NavDropdown eventKey={1} title={username} id="basic-nav-dropdown">
          <MenuItem eventKey={1.1} onClick={onUserChange}>Admin</MenuItem>
          <MenuItem divider />
          <MenuItem eventKey={1.2} onClick={onUserChange}>Registrar</MenuItem>
        </NavDropdown>
      </Nav>
    </Navbar>
    );
};

Header.propTypes = {
  username: PropTypes.string.isRequired,
  onUserChange: PropTypes.func.isRequired
};

export default Header;
