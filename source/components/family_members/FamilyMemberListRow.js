import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import { Button, Panel } from 'react-bootstrap';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';

const FamilyMemberListRow = ({
  idx, familyMember, allRelationships, allNationalities, disableForm,
  onChange, onDeleteFamilyMember
}) => {
  return (
    <Panel>
      <TextInput
        name="firstName" label="First Name" value={familyMember.firstName}
        onChange={onChange} disabled={disableForm}
      />

      <TextInput
          name="lastName" label="Last Name" value={familyMember.lastName}
          onChange={onChange} disabled={disableForm}
      />

      <SelectInput
        name="relationship" label="Relationship" value={familyMember.relationship}
        defaultOption="Select Relationship" options={allRelationships}
        onChange={onChange} disabled={disableForm}
      />

      <SelectInput
        name="nationality" label="Nationality" value={familyMember.nationality.ID}
        defaultOption="Select Nationality" options={allNationalities}
        onChange={onChange} disabled={disableForm}
      />

      <div className="btn-toolbar" role="toolbar">
        <div className="btn-group pull-right">
          <Button
            disabled={!!disableForm}
            data-key={idx}
            type="button"
            bsStyle="danger"
            onClick={onDeleteFamilyMember}>
            Delete
          </Button>
        </div>
      </div>
    </Panel>
  );
};

FamilyMemberListRow.propTypes = {
  idx: PropTypes.number,
  familyMember: PropTypes.object.isRequired,
  allNationalities: PropTypes.array.isRequired,
  allRelationships: PropTypes.array.isRequired,
  onDeleteFamilyMember: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  disableForm: PropTypes.string
};

export default FamilyMemberListRow;
