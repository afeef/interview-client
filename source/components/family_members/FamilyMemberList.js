import React, { PropTypes } from 'react';
import { Table, Button } from 'react-bootstrap';
import FamilyMemberListRow from './FamilyMemberListRow';

const FamilyMemberList = ({
  familyMembers, allRelationships, allNationalities, disableForm,
  onAddNewFamilyMember, onChange, onDeleteFamilyMember
}) => {
  return (
    <div>
      {familyMembers.map((familyMember, index) =>
        <FamilyMemberListRow
          key={index} idx={index}
          familyMember={familyMember}
          allRelationships={allRelationships}
          allNationalities={allNationalities}
          onChange={onChange}
          onDeleteFamilyMember={onDeleteFamilyMember}
          disableForm={disableForm}
        />
      )}
      <div className="btn-toolbar" role="toolbar">
        <div className="btn-group pull-right">
          <Button
            disabled={!!disableForm}
            type="button"
            bsStyle="primary"
            onClick={onAddNewFamilyMember}>
            Add family member
          </Button>
        </div>
      </div>
    </div>
  );
};

FamilyMemberList.propTypes = {
  familyMembers: PropTypes.array.isRequired,
  allNationalities: PropTypes.array.isRequired,
  allRelationships: PropTypes.array.isRequired,
  onDeleteFamilyMember: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onAddNewFamilyMember: PropTypes.func.isRequired,
  disableForm: PropTypes.string
};

export default FamilyMemberList;
