import React, {PropTypes} from 'react';
import {Link} from 'react-router';

const StudentListRow = ({student, onClick}) => {
  return (
    <tr id={student.ID} onClick={onClick}>
      <td><Link to={'/student/' + student.ID}>{student.ID}</Link></td>
      <td>{student.firstName}</td>
      <td>{student.lastName}</td>
      <td>{student.dateOfBirth}</td>
    </tr>
  );
};

StudentListRow.propTypes = {
  student: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired
};

export default StudentListRow;
