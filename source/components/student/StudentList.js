import React, { PropTypes } from 'react';
import { Table } from 'react-bootstrap';
import StudentListRow from './StudentListRow';

const StudentList = ({students, onClick}) => {
  return (
    <Table striped bordered condensed hover>
      <thead>
        <tr>
          <th>Student Id</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Date of Birth</th>
        </tr>
      </thead>
      <tbody>
        {students.map(student =>
          <StudentListRow
            key={student.ID}
            student={student}
            onClick={onClick}
          />
        )}
      </tbody>
    </Table>
  );
};

StudentList.propTypes = {
  students: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired
};

export default StudentList;
