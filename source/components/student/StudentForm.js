import React, {PropTypes} from 'react';
import { Button, Panel } from 'react-bootstrap';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';
import FamilyMemberList from '../family_members/FamilyMemberList';


const StudentForm = ({
  student, familyMembers, allRelationships, allNationalities, disableForm,
  onSave, onChange, onCancel, onDeleteFamilyMember, onAddNewFamilyMember, saving,
}) => {
  return (
    <form>
      <Panel header="Basic information">
        <TextInput
          name="firstName" label="First Name" value={student.firstName}
          onChange={onChange} disabled={disableForm}
        />

        <TextInput
          name="lastName" label="Last Name" value={student.lastName}
          onChange={onChange} disabled={disableForm}
        />

        <TextInput
          name="dateOfBirth" label="Date of Birth" value={student.dateOfBirth}
          onChange={onChange} disabled={disableForm}
        />

        <SelectInput
          name="nationality" label="Nationality" value={student.nationality}
          defaultOption="Select Nationality" options={allNationalities}
          onChange={onChange} disabled={disableForm}
        />
      </Panel>
      <Panel header="Family information" >
        <FamilyMemberList
          familyMembers={familyMembers}
          allRelationships={allRelationships}
          allNationalities={allNationalities}
          onChange={onChange}
          onDeleteFamilyMember={onDeleteFamilyMember}
          onAddNewFamilyMember={onAddNewFamilyMember}
          disableForm={disableForm}
        />
      </Panel>
      <Panel>
        <div className="btn-toolbar" role="toolbar">
          <div className="btn-group pull-right">
            <input
              type="submit" disabled={saving || disableForm}
              value={saving ? 'Saving ...' : 'Save'}
              className="btn btn-primary"
              onClick={onSave} />
            <Button
              type="button"
              bsStyle="default"
              onClick={onCancel}>
              Cancel
            </Button>
          </div>
        </div>
      </Panel>
    </form>
  );
};


StudentForm.propTypes = {
  student : PropTypes.object.isRequired,
  familyMembers: PropTypes.array.isRequired,
  allRelationships: PropTypes.array.isRequired,
  allNationalities: PropTypes.array.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  onDeleteFamilyMember: PropTypes.func.isRequired,
  onAddNewFamilyMember: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  disableForm: PropTypes.string
};

export default StudentForm;
