import React, { PropTypes } from 'react';
import {connect} from 'react-redux';
import { Modal, Button } from 'react-bootstrap';
import StudentForm from './StudentForm';

const StudentModal = ({
  student, familyMembers, allRelationships, allNationalities, showModal, disableForm,
  onSave, onChange, onCancel, onDeleteFamilyMember, onAddNewFamilyMember, errors, saving
}) => {
    return (
      <Modal show={showModal}>
        <Modal.Header>
          <h2>Student Detail</h2>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-xs-12">
              <StudentForm
                disableForm={disableForm}
                student={student}
                familyMembers={familyMembers}
                allRelationships={allRelationships}
                allNationalities={allNationalities}
                onChange={onChange}
                onSave={onSave}
                onCancel={onCancel}
                onDeleteFamilyMember={onDeleteFamilyMember}
                onAddNewFamilyMember={onAddNewFamilyMember}
                saving={saving}
              />
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
};

StudentModal.propTypes = {
  student: PropTypes.object,
  familyMembers: PropTypes.array.isRequired,
  allRelationships: PropTypes.array.isRequired,
  allNationalities: PropTypes.array.isRequired,
  showModal: PropTypes.bool.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  onDeleteFamilyMember: PropTypes.func.isRequired,
  onAddNewFamilyMember: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  disableForm: PropTypes.string
};


export default StudentModal;
