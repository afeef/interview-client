import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import toastr from 'toastr';
import { Button, Panel } from 'react-bootstrap';
import * as studentActions from '../../actions/studentActions';
import * as familyMemberActions from '../../actions/familyMemberActions';
import * as headerActions from '../../actions/headerActions';
import StudentList from './StudentList';
import StudentModal from './StudentModal';
import Header from '../common/Header';

class StudentsPage extends React.Component {
  constructor(props, context){
    super(props, context);

    this.state = {
      student: {},
      showModal: false,
      saving: false,
      newStudent: false
    };

    this.handleStudentClick = this.handleStudentClick.bind(this);
    this.handleModalCancel = this.handleModalCancel.bind(this);
    this.handleAddStudentClick = this.handleAddStudentClick.bind(this);
    this.handleAddNewFamilyMember = this.handleAddNewFamilyMember.bind(this);
    this.handleDeleteFamilyMember = this.handleDeleteFamilyMember.bind(this);
    this.handleUserChange = this.handleUserChange.bind(this);
    this.saveStudent = this.saveStudent.bind(this);
    this.updateStudentState = this.updateStudentState.bind(this);
  }

  getStudentById(students, id){
    const student = students.filter(student => student.ID === id);

    if(student.length)
      return student[0];

    return null;
  }

  handleUserChange(e) {
    this.props.headerActions.switchUser(e.target.innerHTML);
  }

  handleDeleteFamilyMember(e) {
    this.props.familyMemberActions.deleteFamilyMember(e.target.getAttribute('data-key'));
  }

  handleAddNewFamilyMember() {
    this.props.familyMemberActions.addNewFamilyMember();
  }

  handleAddStudentClick() {
    this.setState({ student: {}, showModal: true, newStudent: true});
  }

  handleStudentClick(e) {
    e.preventDefault();
    const studentId = parseInt(e.target.parentElement.getAttribute('id'));
    const student = this.getStudentById(this.props.students, studentId);
    this.setState({
      student: student,
      showModal: true
    });

    this.props.familyMemberActions.loadStudentFamilyMembers(studentId);
  }

  handleModalCancel(){
    this.setState({ showModal: false, newStudent: false });
  }

  saveStudent(e) {
    e.preventDefault();
    this.setState({saving: true});
    this.props.studentActions.saveStudent(
      this.state.student
    ).then(()=>this.saveStudentSuccess())
    .catch(error => {
      toastr.error(error);
      this.setState({saving: false});
    });
  }

  saveStudentSuccess(){
    this.setState({saving: false, showModal: false, newStudent: false});
    toastr.success('Student saved');
  }

  updateStudentState(e){
    const field = e.target.name;
    let student = this.state.student;
    student[field] = e.target.value;

    return this.setState({student: student});
  }

  render () {
    const { students, nationalities, familyMembers, relationships, username } = this.props;
    const { student, isStudentAdded, showModal, saving, errors, newStudent} = this.state;
    const disabled = (!newStudent && username === 'Admin') ? 'disabled' : '';

    return (
      <div>
        <Header username={username} onUserChange={this.handleUserChange} />
        <h1>Students</h1>
        <Panel className="pull-right">
          <Button
            type="button"
            bsStyle="default"
            onClick={this.handleAddStudentClick}>
            Add Student
          </Button>
        </Panel>
        <Panel>

          <StudentList
            students={students}
            onClick={this.handleStudentClick} />

          <StudentModal
            disableForm={disabled}
            student={student}
            familyMembers={familyMembers}
            allRelationships={relationships}
            allNationalities={nationalities}
            showModal={showModal}
            onSave={this.saveStudent}
            onChange={this.updateStudentState}
            onCancel={this.handleModalCancel}
            onDeleteFamilyMember={this.handleDeleteFamilyMember}
            onAddNewFamilyMember={this.handleAddNewFamilyMember}
            saving={saving}
          />
        </Panel>
      </div>
    );
  }
}

StudentsPage.propTypes = {
  students : PropTypes.array.isRequired,
  familyMembers: PropTypes.array.isRequired,
  relationships: PropTypes.array.isRequired,
  headerActions: PropTypes.object.isRequired,
  studentActions: PropTypes.object.isRequired,
  familyMemberActions: PropTypes.object.isRequired,
  nationalities: PropTypes.array.isRequired,
  username: PropTypes.string.isRequired
};

function mapStateToProps(state, ownProps){
  const relationships = [
    {value: 'Parent', text: 'Parent'},
    {value: 'Sibling', text: 'Sibling'},
    {value: 'Spouse', text: 'Spouse'}
  ];

  const nationalitiesFormattedForDropdown = state.nationalities.map(nationality => {
      return {
        value: nationality.ID,
        text: nationality.Title
      };
  });

  return {
    students: state.students,
    familyMembers: state.familyMembers,
    relationships: relationships,
    nationalities: nationalitiesFormattedForDropdown,
    username: state.header
  };
}

function mapDispatchToProps(dispatch){
  return {
    headerActions: bindActionCreators(headerActions, dispatch),
    studentActions: bindActionCreators(studentActions, dispatch),
    familyMemberActions: bindActionCreators(familyMemberActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(StudentsPage);
