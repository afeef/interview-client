# README #

This document describes in short how to run this application
### Running the app ###

* npm install && npm start

### Building the app ###

* npm install && npm run build

### Assumptions ###

* This application assumes that API server is running at http://localhost:8088/. If required change the base API_URL in source/api/config.js

### Remaining work ###

* A few bugs on the modal dialog
* A consolidated ajax request for saving/updating the detailed student info
